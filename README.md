# Anuket-docs

Documentación de __Anuket__, Awesome [Google maps](https://developers.google.com/maps/?hl=es-419) Utils.


## Versión estable
La documentación de __Anuket__(este repo) está disponible de [forma online.](https://diniremix.web.app/projects/anuket/)

Puedes descargar la versión estable del proyecto [por aquí](https://gitlab.com/HomeInside/Anuket), ó revisa los lanzamientos mas recientes, [por aquí](https://gitlab.com/HomeInside/Anuket/-/tags)


## Listado de Cambios
El listado completo de cambios (changelog), [por aquí](https://gitlab.com/HomeInside/Anuket/-/blob/master/CHANGELOG.md)


## Licencia
El texto completo de la licencia puede ser encontrado en el archivo __MIT-LICENSE.txt__.


## Contacto

- [Diniremix](https://gitlab.com/diniremix)
- [Duverney](https://gitlab.com/duverney.ortiz)
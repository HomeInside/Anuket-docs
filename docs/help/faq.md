# FAQ :loudspeaker:

A continuación exponemos una lista de preguntas y respuestas comunes, que pueden serte de ayuda.

- __¿Por qué Anuket__?

¿Por qué no?.

- __¿Qué versión de Javascript Utiliza?__

[ECMAScript 5.1](https://www.w3schools.com/js/js_versions.asp) (A.K.A. ES5)

- __¿Por qué utilizar ECMASicript 5, y no TypeScript, CoffeeScript, Flow, etc...?__

Al utilizar ECMAScript 5.1, es [Javascript vanilla](https://plainjs.com), el de toda la vida, puede ejecutarse en cualquier navegador actual, sin necesidad de pasarlo por un transpilador (Ej: Babel) y al estar desarrollado como un modulo [__IIFE__](https://developer.mozilla.org/es/docs/Glossary/IIFE) permite su integración en cualquier sitio web, ó frameworks Javascript actuales (Vue, Angular, Ember, Backbone etc...) sin configuraciones adicionales.

- __¿Donde empezar?__

[Explora la Documentación](/#anuket).

- __¿Cero configuración?__

Si, una vez hecha la [instalación](../guide/installation.md) y solventado los [requisitos](../guide/requirements.md) es tan sencillo como:

Crear el contenedor del mapa:
```html
<div id="themap"></div>
```

Inicializar __Anuket:__
```js
Anuket.run('#themap');
```

- __¿El mapa no carga, ó no se muestra correctamente, que puede haber pasado?__

Asegurate de que los [requisitos esten cumplidos](../guide/requirements.md) y que hayas [iniciado bien Anuket](../modules/run.md).
Adicionalmente recuerda proveer [una clave de API para Maps](https://developers.google.com/maps/documentation/javascript/adding-a-google-map?hl=es-419#key).

- __¿La documentación esta actualizada con respecto a la ultima version de Anuket?__

La documentación aquí descrita corresponderá con la rama [master](https://gitlab.com/HomeInside/Anuket/-/branches) del proyecto.

- __¿En que está desarrollada la documentación de este sitio?__

Buscamos buenas alternativas como [Docsify](https://docsify.js.org/#/), [Docusaurus](https://docusaurus.io) y [VuePress](https://vuepress.vuejs.org), sin embargo preferimos a [Material for MkDocs](https://squidfunk.github.io/mkdocs-material/).

- __¿Esta documentación abarca las funcionalidades de Google Maps API?__

No, no todas ellas, solo las descritas en los [métodos públicos](../modules/methods.md).

- __¿Puedo agregar una funcionalidad nueva (Ej: Plugin) de forma sencilla?__

Por supuesto, Para evitar que __Anuket__ se hinche demasiado para usarla, la incorporación de características adicionales, se encuentra cubierta en el apartado de [extensiones](../extensions/extensions.md).

- __¿Como puedo contribuir?__

Recomendamos leer la [guía de estilo de Javascript de Airbnb](https://github.com/airbnb/javascript) y revisar el apartado de [Referencias](references.md)

Para cualquier duda, comentario, sugerencia ó aporte, dirigete a la sección de [issues.](https://gitlab.com/HomeInside/Anuket/-/issues)
Antes de abrir un issue nuevo, revisa los ya existentes en busca de una solución (posiblemente ya planteada) para el problema que se te presenta.

Chequea la [lista de cambios](CHANGELOG.md) y las [novedades de este proyecto](https://gitlab.com/HomeInside/Anuket).
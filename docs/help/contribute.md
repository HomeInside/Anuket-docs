# Como Contribuir :tada:

Recomendamos leer la [guía de estilo de Javascript de Airbnb](https://github.com/airbnb/javascript) y revisar el apartado de [Referencias](references.md)

Tú colaboración es muy valiosa, agradecemos que dediques algo de tu tiempo en las siguientes actividades:

- Crear [nuevas funcionalidades](../modules/methods.md) ó [extensiones](../extensions/extensions.md).
- Mantener esta documentación lo más [actualizada posible](https://gitlab.com/HomeInside/Anuket-docs).
- [Informar fallos](https://gitlab.com/HomeInside/Anuket/-/issues).
- Traducciones.
- [Lista de cambios](CHANGELOG.md).

Para cualquier duda, comentario, sugerencia ó aporte, dirigete a la sección de [issues.](https://gitlab.com/HomeInside/Anuket/-/issues)

!!! tip
    Antes de abrir un issue nuevo, revisa los ya existentes en busca de una solución (posiblemente ya planteada) para el problema que se te presenta.

## Inicio rápido

```bash
git clone git@gitlab.com:HomeInside/Anuket.git && cd Anuket
```


## Extensiones

En el apartado de [extensiones](../extensions/extensions.md), encontrarás una guía sencilla para extender __Anuket__ con más funcionalidades y cubrir alguna necesidad puntual de tu proyecto. 
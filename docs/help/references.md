# Referencias :pushpin:

- [Airbnb JavaScript Style Guide](https://github.com/airbnb/javascript)

- [JavaScript Design Patterns](https://addyosmani.com/resources/essentialjsdesignpatterns/book/index.html#designpatternsjavascript)

- [Mastering the Module Pattern](https://toddmotto.com/mastering-the-module-pattern/)

- [Google Maps JavaScript API V3 Reference](https://developers.google.com/maps/documentation/javascript/reference/3.exp/?hl=es-419)

- [Gmaps documentation.](http://hpneo.github.io/gmaps/documentation.html)

- [Demystifying JavaScript Closures, Callbacks and IIFEs.](https://www.sitepoint.com/demystifying-javascript-closures-callbacks-iifes/)

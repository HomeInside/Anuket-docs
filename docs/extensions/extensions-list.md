# Listado de Extensiones :rocket: :zap:

Como te [comentamos anteriormente](extensions.md), la forma más sencilla, de agregar nuevas funcionalidades a __Anuket__, es mediante la creación de un modulo [__IIFE__](https://developer.mozilla.org/es/docs/Glossary/IIFE).

Las extensiones proveen nuevas funcionalidades de forma separada del núcleo principal de __Anuket__.

Esperamos que sean de utilidad para tus proyectos.


## Extensiones disponibles

- [__decodePolyline__](https://gitlab.com/HomeInside/Anuket/blob/develop/plugins/decodePolyline.js)Permite decodificar rutas de polilínea de acuerdo con el [Algoritmo de polilínea codificada](https://developers.google.com/maps/documentation/utilities/polylinealgorithm).
- [__distancematrix__](https://gitlab.com/HomeInside/Anuket/blob/develop/plugins/distancematrix.js) Calcula la distancia de viaje y la duración del viaje entre múltiples orígenes y destinos utilizando un modo de viaje determinado.
- [__geojson__](https://gitlab.com/HomeInside/Anuket/blob/develop/plugins/geojson.js) facilita la visualización de datos en formato [GeoJSON](https://es.wikipedia.org/wiki/GeoJSON), y mostrar puntos, cadenas de líneas y polígonos.
- [__kml__](https://gitlab.com/HomeInside/Anuket/blob/develop/plugins/kml.js)facilita la visualización de datos en formato [KML](https://es.wikipedia.org/wiki/KML), y mostrar puntos, cadenas de líneas y polígonos.
- [__osmMap__](https://gitlab.com/HomeInside/Anuket/blob/develop/plugins/osmMap.js) Permite utilizar una capa base de [OpenStreetMap](https://www.openstreetmap.org/about)
- [__staticMap__](https://gitlab.com/HomeInside/Anuket/-/blob/develop/plugins/staticMap.js) Permite generar una imagen (GIF, PNG o JPEG) del mapa de Google, usando Maps Static API.

!!! note "Nota:"
    Te recomendamos pasar por el apartado de [Referencias](../help/references.md), en especial el artículo de `Mastering the Module Pattern`, de [Todd Motto.](https://toddmotto.com/mastering-the-module-pattern/)
# getAnuketObj

Devuelve un objeto que contiene métodos internos de __Anuket__ para el manejo del mapa base, se establece al inicio de la ejecución (método [run](run.md)).


## Lista de Parámetros:

- `Ninguno`


## Definición:

```js
fn.getAnuketObj = function() {};
```


## Un ejemplo:

```js
Anuket.getAnuketObj();
```
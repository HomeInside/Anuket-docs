# Inicio Rápido

Como se comentó [anteriormente](../guide/getting-started.md), __Anuket__ cuenta con una gran biblioteca de métodos que están disponibles de inmediato, A continuación se citan los métodos disponibles por defecto:

- [Métodos Disponibles](methods.md#metodos-disponibles)
- [Métodos Internos](methods.md#listado-de-metodos-internos)
# getGoogleObj

Devuelve un objeto que contiene las funciones de `google.maps.Map`, se establece al inicio de la ejecución (método [run](run.md)).


## Lista de Parámetros:

- `Ninguno`


## Definición:

```js
fn.getGoogleObj = function() {};
```


## Un ejemplo:

```js
Anuket.getGoogleObj();
```
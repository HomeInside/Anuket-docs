# setMapType

Permite definir un tipo de mapa. Existen cuatro tipos de mapas disponibles:

- __hybrid__
- __roadmap__
- __satellite__
- __terrain__


## Lista de Parámetros:

- __el__: `string` __default:__ `null`


## Definición:

```js
fn.setMapType = function (el) {};
```


## Ejemplo:

```js
Anuket.setMapType('satellite');
```


## Consideraciones:

- El parámetro `el`( para el tipo de mapa) acepta sólo los tipos de mapas definidos anteriormente, en caso de que la opción enviada no sea válida, se establecerá `roadmap` como tipo de mapa por defecto.

!!! example "Extendiendo"
    Este método se puede extender mediante el uso de plugins[(extensiones)](../extensions/extensions.md) y definir muchos tipos de mapas, a partir de servicios de mapas externos, como [OpenStreetMap](https://www.openstreetmap.org/about). Revisa la [lista de extensiones,](../extensions/extensions-list.md) para más información. 

!!! note "Nota:"
    Recomendamos leer el apartado de [Tipos de mapas](https://developers.google.com/maps/documentation/javascript/maptypes?hl=es-419) y [mapas con ajustes de estilo](https://developers.google.com/maps/documentation/javascript/styling?hl=es-419) para más información.

  
# drawCircle :new:

Permite dibujar un círculo en el mapa:

!!! note "Nota:"
    Este método, sólo se encuentra disponible en la version __3.0.b.__ ó superior.


## Lista de Parámetros:
- __coords__: `object` __default:__ `null`
- __radius__: `integer` __default:__ `100`
- __options:__ un `objeto` con opciones adicionales (este campo __no__ es requerido).
    + strokeColor: `string` __default:__ `null`
    + strokeOpacity: `integer` __default:__ `null`
    + strokeWeight: `integer` __default:__ `null`
    + fillColor: `string` __default:__ `null`
    + fillOpacity: `float` __default:__ `null`
    + toRemove: `string` __default:__ `null`
    + editable: `boolean` __default:__ `false`
    + draggable: `boolean` __default:__ `false`
  

## Definición:

```js
fn.drawCircle = function(coords, radius, options) {};
```


## Ejemplos:

- Con las opciones por defecto:

```js
var coords = {lat: 51.51455, lng: -0.07306};
var radius = 100;

Anuket.drawCircle(coords, radius);
```

- Definiendo opciones:

```js
var options = {
  strokeColor: '#FF0000',
  strokeOpacity: 0.8,
  strokeWeight: 2,
  fillColor: '#FF0000',
  fillOpacity: 0.35,
  toRemove: null,
  editable: false,
  draggable: false,
};

Anuket.drawCircle(coords, radius, options);
```


## Consideraciones:

- La opción `radius` permite definir el radio del círculo, en metros.

- La opción `strokeColor` permite cambiar el color de contorno, del círculo, en formato hexadecimal.

- La opción `strokeOpacity` permite cambiar la opacidad del color de contorno.

- La opción `strokeWeight` permite cambiar el ancho de la línea de contorno, en píxeles.

- La opción `fillColor` permite cambiar el color de relleno del círculo, en formato hexadecimal.

- La opción `fillOpacity` permite cambiar la opacidad del color de relleno, del círculo, en valores de __0.0__ a __1.0__.

- La opción `toRemove` acepta: __all__, __polygons__ ó __polylines__. Dependiendo la opción escogida, el método __drawCircle__ ejecutará las siguientes acciones:
    + __all__: Limpiará el mapa.
    + __polygons__: Borrará todos los polígonos existentes.
    + __polylines__: Borrará todas las polilíneas existentes.

    antes de dibujar el nuevo círculo.

- La opción `editable` del círculo, especifica si se puede editar directamente en el mapa.

- La opción `draggable` del círculo, permitir arrastrar el círculo sobre el mapa.


!!! warning "Advertencia:"
    Las opciones `editable` y `draggable` estarán disponibles solo en la versión __Beta__, es posible que sean eliminadas en el proximo lanzamiento de la versión estable, ó en futuras versiones de __Anuket__.


!!! note "Nota:"
    Recomendamos leer el apartado de [Círculos en Google Maps](https://developers.google.com/maps/documentation/javascript/shapes#circles) y [la Api asociada](https://developers.google.com/maps/documentation/javascript/reference/#Circle) para más información.

  
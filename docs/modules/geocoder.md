# geocoder

Permite georreferenciar una dirección, teniendo en cuenta lo siguiente:

- La dirección debe ser un `string` con el formato:
  - __`'direccion, ciudad'`__: Ejemplo: __`'10 Whitechapel High St, london'`__
  - __`'latitud, longitud'`__: Ejemplo: __`'51.51455, -0.07306'`__


## Lista de Parámetros:

El método __geocoder__ recibe 3 parametros:

  - __address:__ `string` __default:__ `null`
  - __callBack:__ `function` __default:__ `null` (ver detalles más abajo.)
  - __options:__ un `objeto` con opciones adicionales (este campo __no__ es requerido).
      + centerMap: `boolean` __default:__ `null`
      + country: `string` __default:__ `false`
      + marker: `true` __default:__ `null`
      + zoom: `integer` __default:__ `null`


## Definición:

```js
fn.geocoder = function(address, callBack, options) {};
```



## Ejemplos:

- Con las opciones por defecto:

```js
Anuket.geocoder('10 Whitechapel High St, london', function(coords, results, status) {
  console.log('coords', coords);
  console.log('results', results);
  console.log('status', status);
});
```

- Definiendo opciones:

```js
var options = {
  country: 'GB',
  centerMap: true,
  zoom: 14,
  marker: true
}

Anuket.geocoder('10 Whitechapel High St, london', function(coords, results, status) {
  console.log('coords', coords);
  console.log('results', results);
  console.log('status', status);
}, options);
```


## Consideraciones:

- La opción `centerMap` solo acepta **true** ó **false**, permite centrar el mapa.

- La opción `country` acepta un nombre de país ó un código de país bajo el estandar [ISO 3166-1](https://es.wikipedia.org/wiki/ISO_3166-1) de dos letras, permite aplicar un filtro por país a la busqueda, en caso de que no se envíe, la busqueda será global, [ más información por aquí](https://developers.google.com/maps/documentation/javascript/geocoding#ComponentFiltering).

- La opción `marker` solo acepta **true** ó **false**, permite establecer un marcador en el mapa.

- La opción `zoom` solo acepta un valor numérico (enteros positivos) de **0** a **18**, permitiendo establecer el nivel de zoom en el mapa.

- Las opciones `centerMap` y `marker` utilizan las coordenadas devueltas por el servicio de georreferenciación.

!!! note "Nota:"
    - Las opciones anteriores se aplicarán, sólo si el resultado de la georreferenciación es satisfactorio.

- Si la georreferenciación falla, se devolverá un [código de error, definido en la documentación de Google Maps Api](https://developers.google.com/maps/documentation/javascript/geocoding#GeocodingStatusCodes).

- El parámetro `callBack` es una función anónima, que se ejecuta **al finalizar el servicio de la georreferenciación**, devuelve tres parametros:
    - __coords:__ Un objeto con el **primer resultado** de la georreferenciación.
    - __results:__ Un objeto con **todos los resultados** de la georreferenciación.
    - __status:__ Un mensaje con el **código del resultado** obtenido.

!!! note "Nota:"
    Recomendamos leer el apartado de [Geocodificación](https://developers.google.com/maps/documentation/geocoding/intro?hl=es-419) y [Filtrado de componentes](https://developers.google.com/maps/documentation/javascript/geocoding#ComponentFiltering) para más información.
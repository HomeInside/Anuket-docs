# fitBounds :new:

Permite establecer los límites dados, por un Objeto espacial (marcador, círculo, polígono, polilínea) en el mapa, centrandolo y fijando un zoom sobre dichos elementos:

!!! note "Nota:"
    Este método, sólo se encuentra disponible en la version __3.0.b.__ ó superior.


## Lista de Parámetros:
  + __shape__: `object` __default:__ `null`
  + __type__: `string` __default:__ `null`
  

## Definición:

```js
fn.fitBounds = function(shape, type) {};
```


## Un ejemplo:

```js
var newMarker = Anuket.addMarker({lat: 51.51455, lng: -0.07306});

Anuket.fitBounds(newMarker, 'marker');
```


## Consideraciones:

- La opción `shape` acepta objetos de tipo: [__marcador__](add-markers.md), [__circulo__](draw-circle.md), [__polígono.__](draw-polygon.md) ó [__polilínea.__](draw-polyline.md)

- La opción `type` acepta las siguientes opciones:
    + __marker__
    + __circle__
    + __polygon__
    + __polyline__


!!! note "Nota:"
    Recomendamos leer el apartado de [fitBounds](https://developers.google.com/maps/documentation/javascript/reference/map#Map.fitBounds) y [LatLngBounds](https://developers.google.com/maps/documentation/javascript/reference/coordinates#LatLngBounds) para más información.

  
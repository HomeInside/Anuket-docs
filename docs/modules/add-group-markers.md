# addGroupMarkers :new:

Agrega una colección de marcadores en el mapa, con las mismas opciones utilizadas en el método [addMarker](add-markers.md).

!!! danger ":pushpin: Pruebas :fire:"
    Este método es relativamente reciente y se encuentra en pruebas, por lo que se recomienda su uso con precaución.


## Lista de Parámetros:

El método __addGroupMarkers__ espera un solo parametro:

- __markerList__: `Array` __default:__ `[]` un array de objetos con los siguientes elementos
    + lat: `float` __default:__ `null`
    + lng: `float` __default:__ `null`
    + icon: `string` __default:__ `null` (este campo no es requerido).
    + title: `string` __default:__ `null` (este campo no es requerido).
    + label: `object` __default:__ `null` un objeto con opciones adicionales (este campo no es requerido).
        * text: `string` __default:__ `null`
        * color: `string` __default:__ `null`
        * fontSize: `string` __default:__ `null`
    + click: `function` __default:__ `null` (este campo no es requerido).


## Definición:

```js
fn.addGroupMarkers = function(markerList) {};
```

## Ejemplos:

- Con las opciones por defecto:

```js
var markersData = [
  {
    lat: 51.51455,
    lng: -0.07306
  },
  {
   lat: 51.51800,
   lng: -0.76370
  }
];

Anuket.addGroupMarkers(markersData);
```

- Definiendo opciones adicionales:

```js
var markersData = [
  {
    lat: 51.51455,
    lng: -0.07306,
    icon: '/path/to/icon',
    title: 'marker #1',
    label: {
      text: 'A',
      color: "#fff",
      fontSize: "3.0px"
    }
  },
  {
    lat: 51.51800,
    lng: -0.76370,
    icon: '/path/to/another/icon',
    title: 'marker #2',
    label: {
      text: 'B',
      color: "#fff",
      fontSize: "3.0px"
    },
    click: function(e){
      console.info('event:', e);
      var position = e.getPosition();
      console.log('latitude:', position.lat(), 'longitude', position.lng());
    }
  }
];

Anuket.addGroupMarkers(markersData);
```


## Consideraciones:

- Como se comentó arriba, el método **addGroupMarkers**, utiliza los mismos campos del parámetro **options** (con algunas excepciones), del método [addMarker](add-markers.md), para la creación del grupo de marcadores.

- En cada objeto, dentro del Array de marcadores, solo los valores con las coordenadas (`lat` y `lng`) **son requeridos**, los demás, son opcionales, por lo tanto pueden enviarse, los que considere necesarios, como se observa en los ejemplos anteriores.

???+ warning "En Revisión:"
    - Opciones no disponibles y/o deshabilitadas:
    
        + __`draggable`__ la cual permite establecer si el marcador podrá ser arrastrable en el mapa, por defecto siempre es **false**.
      
        + __`onDragend`__ deshabilitada debido a que la opción `draggable` está fijada en  **false**.
      
        + __`onClick`__ esta opción es reemplazada por `click` como se muestra en el ejemplo anterior.


!!! note "Nota:"
    Recomendamos leer el apartado de [Marcadores](https://developers.google.com/maps/documentation/javascript/markers#introduction) y [Eventos](https://developers.google.com/maps/documentation/javascript/events?hl=es-419) para más información.
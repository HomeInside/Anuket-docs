# addMarker

Agrega un marcador en el mapa.


## Lista de Parámetros:
El método __addMarker__ espera dos parametros:

- __latlong__: `object` __default:__ `null`
- __options:__ un `objeto` con opciones adicionales (este campo __no__ es requerido).
    + draggable: `boolean` __default:__ `false`
    + icon: `string` __default:__ `null`
    + title: `string` __default:__ `null`
    + label: `string` __default:__ `null`
    + infoWindow: `string` __default:__ `null`
    + onDragend: `function` __default:__ `null`
    + onClick: `function` __default:__ `null`


## Definición:

```js
fn.addMarker = function(latlong, options) {};
```

## Ejemplos:

- Con las opciones por defecto:

```js
Anuket.addMarker({lat: 51.51455, lng: -0.07306});
```

- Definiendo opciones:

```js
var options = {
  draggable: true,
  icon: '/path/to/icon',
  title: 'Current position',
  infoWindow: '<p>HTML Content</p>',
  onDragend: function(e, coords){
    console.info('event:', e);
    console.info('coords:', coords);
  },
  onClick: function(e, coords){
    console.info('event:', e);
    console.info('coords:', coords);
  }
};
  
Anuket.addMarker({"lat": 51.51455, "lng": -0.07306}, options);
```


## Consideraciones:

- Como se comentó arriba, el objeto **options** y sus valores son opcionales, por lo tanto pueden enviarse los que considere necesarios.

- La opción `draggable` solo acepta **true** ó **false**, permite establecer si el marcador podrá ser arrastrable en el mapa.

- La opción `icon` permite cambiar el icono del marcador, sino se establece, por defecto, usará el [icono predeterminado de Google Maps](https://developers.google.com/maps/documentation/javascript/markers#simple_icons).

- La opción `title` permite cambiar el título del marcador, es visible, al pasar el cursor encima de este.

???+ warning "En Revisión:"
    - La opción `label` permite establecer una etiqueta al marcador, el label es una letra o número que aparece dentro del marcador.

- La opción `infoWindow` permite establecer una ventana emergente sobre el mapa con contenido (generalmente texto o imágenes).

- La opción `onDragend` es una función anonima, que se ejecuta **al arrastrar el marcador sobre el mapa** (Esta función solo se activa cuando la opcion `draggable` esta en **true**), y devuelve dos parametros:
    + El objeto del evento `dragend` del marcador.
    + Un objeto con las nuevas coordenadas del marcador, obtenidas al finalizar el evento de arrastre.

- La opción `onClick` es una función anonima, que se ejecuta **al hacer click en el marcador**, devuelve dos parametros:
    + El objeto del evento `click` del marcador.
    + Un objeto con las coordenadas del mismo.


!!! note "Nota:"
    Recomendamos leer el apartado de [Marcadores,](https://developers.google.com/maps/documentation/javascript/markers#introduction) y [Eventos](https://developers.google.com/maps/documentation/javascript/events?hl=es-419) para más información.
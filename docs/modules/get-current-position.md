# getCurrentPosition

La API de geolocalización HTML, se utiliza para obtener la posición geográfica de un usuario, como esto puede comprometer la privacidad, la posición no está disponible **a menos que el usuario la apruebe.**

!!! note "Nota:"
    La geolocalización es más precisa para dispositivos con GPS, como teléfonos inteligentes.


## Lista de Parámetros:

El método __getCurrentPosition__ recibe un parametro:

- __callback:__ `function` __default:__ `null`


## Definición:

```js
fn.getCurrentPosition = function (callback) {};
```



## Un ejemplo:

```js
Anuket.getCurrentPosition(function(coords, results) {
  console.log('coords:', coords);
  console.log('results:', results);
});
```


## Consideraciones:

- La API de geolocalización se publica a través del objeto [navigator.geolocation.](https://developer.mozilla.org/es/docs/Web/API/Window/navigator/geolocation) Si el objeto existe, los servicios de geolocalización estarán disponibles.
Esto inicia una solicitud asíncrona para detectar la posición del usuario, y consulta el hardware de posicionamiento para obtener información actualizada. Cuando se determina la posición, se ejecuta la función de **callback**.

- El parámetro `callback` es una función anónima, que se ejecuta **al finalizar el servicio y obtener la ubicación actual del usuario**, devuelve dos parametros:
    + __coords:__ Un objeto con las coordenadas obtenidas.
    + __results:__ Un objeto con un mensaje si ocurre algún error, o **null** en caso de que no.

!!! note "Nota:"
    Recomendamos leer el apartado de [la API de geolocalización](https://developer.mozilla.org/es/docs/WebAPI/Using_geolocation) y [su especification](https://www.w3.org/TR/geolocation-API/) para más información.
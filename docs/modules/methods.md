# Métodos Disponibles

Los métodos disponibles de __Anuket__, permiten el acceso a funciones integradas, que proveen funcionalidades como la configuración inicial del mapa, gestión de marcadores, dibujo de polilineas, streetView, georreferenciacion estandar (direcciones), georreferenciacion inversa (coordenadas), ajuste de Zoom, controles, geolocalizacion y demás funcionalidades que estan disponibles usando la API de Google Maps, de un modo más amigable.

## Listado de Métodos Disponibles

- [__addGroupMarker__ :new:](add-group-markers.md#addgroupmarkers)
- [__addMarker__](add-markers.md#addmarker)
- [__centerMap__](center-map.md#centermap)
- [__centerOverOrigin__](center-over-origin.md#centeroverorigin)
- [__clearMap__](clear-map.md#clearmap)
- [__drawRoute__](draw-route.md#drawroute)
- [__drawPolygon__ :new:](draw-polygon.md#drawpolygon-new)
- [__drawPolyline__ :new:](draw-polyline.md)
- [__drawCircle__ :new:](draw-circle.md#drawcircle-new)
- [__fitBounds__ :new:](fit-bounds.md#fitbounds-new)
- [__geocoder__](geocoder.md#geocoder)
- [__getCurrentPosition__](get-current-position.md)
- [__getGoogleObj__](get-google-obj.md#getgoogleobj)
- [__getAnuketObj__](get-anuket-obj.md#getanuketobj)
- [__getOriginPosition__](get-origin-position.md#getoriginposition)
- [__getZoom__](zoom.md#getzoom)
- [__removeMarkers__](remove-markers.md#removemarkers)
- [__run__](run.md#run)
- [__setMapType__](set-map-type.md#setmaptype)
- [__setZoom__](zoom.md#setzoom)
- [__streetView__](street-view.md#streetview)


## Listado de Métodos Internos

Solo está disponible, el método [__getAnuketObj__](get-anuket-obj.md#getanuketobj) para el manejo del mapa base.
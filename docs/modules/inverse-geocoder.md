# inverseGeocoder

Permite traducir una ubicación en el mapa (coordenadas) en una dirección legible, teniendo en cuenta lo siguiente:

- Las coordenadas deben ser un `objeto` con el formato:
    + __`{lat: Float, lng: Float}`__: Ejemplo: __`{lat: 51.51455, lng: -0.07306}`__


## Lista de Parámetros:

El método __inverseGeocoder__ recibe 3 parametros:

- __coords:__ `object` __default:__ `null`
- __callBack:__ `function` __default:__ `null`
- __options:__ un `objeto` con opciones adicionales (este campo __no__ es requerido).
    + centerMap: `boolean` __default:__ `null`
    + marker: `true` __default:__ `null`
    + zoom: `integer` __default:__ `null`


## Definición:

```js
fn.inverseGeocoder = function(coords, callBack, options) {};
```



## Ejemplos:

- Con las opciones por defecto:

```js
Anuket.inverseGeocoder({lat: 51.51455, lng: -0.07306}, function(address, results, status) {
  console.log('formatted address', address);
  console.log('results', results);
  console.log('status', status);
});
```

- Definiendo opciones:

```js
var options = {
  centerMap: true, 
  marker: true,
  zoom: 14
}

Anuket.inverseGeocoder({lat: 51.51455, lng: -0.07306}, function(address, results, status) {
  console.log('formatted address', address);
  console.log('results', results);
  console.log('status', status);
}, options);
```


## Consideraciones:

- Los campos `lat` y `lng` que representan la latitud y la longitud, del objeto **coords**, respectivamente, están en formato númerico con decimales y se recomendia enviar al menos, **5 digitos decimales**, para una mejor precisión en las coordenadas.

- La opción `centerMap` solo acepta **true** ó **false**, permite centrar el mapa.

- La opción `marker` solo acepta **true** ó **false**, permite establecer un marcador en el mapa.

- La opción `zoom` solo acepta un valor numérico (enteros positivos) de **0** a **18**, permitiendo establecer el nivel de zoom en el mapa.

- Las opciones `centerMap` y `marker` utilizan las coordenadas originales enviadas como parámetro.

!!! note "Nota:"
    - Las opciones anteriores se aplicarán, sólo si el resultado de la georreferenciación inversa es satisfactorio.

- Si la georreferenciación inversa falla, se devolverá un [código de error, definido en la documentación de Google Maps Api](https://developers.google.com/maps/documentation/javascript/geocoding#GeocodingStatusCodes).

- El parámetro `callBack` es una función anónima, que se ejecuta **al finalizar el servicio de la georreferenciación inversa**, devuelve tres parametros:
    + __address:__ Un string con el **primer resultado** de la georreferenciación inversa, normalmente, la direccción legible.
    + __results:__ Un objeto con **todos los resultados** de la georreferenciación inversa.
    + __status:__ Un mensaje con el **código del resultado** obtenido.

- La geocodificación inversa no es una ciencia exacta. El geocodificador intentará encontrar la ubicación direccionable más cercana dentro de una cierta tolerancia.

!!! note "Nota:"
    Recomendamos leer el apartado de [Geocodificación inversa](https://developers.google.com/maps/documentation/javascript/geocoding#ReverseGeocoding) y [Filtrado de componentes](https://developers.google.com/maps/documentation/javascript/geocoding#ComponentFiltering) para más información.
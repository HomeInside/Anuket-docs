# removeMarkers

Elimina todos los `marcadores` que se encuentren en el mapa.


## Lista de Parámetros:

- `Ninguno`


## Definición:

```js
fn.removeMarkers = function() {};
```


## Un ejemplo:

```js
Anuket.removeMarkers();
```
# centerMap
Permite centrar el mapa base, en una Localización geográfica diferente a la establecida actualmente.

## Lista de Parámetros:
El método __centerMap__ espera dos parametros:

- __lat__: `float` __default:__ `null`
- __lng__: `float` __default:__ `null`
- __options:__ un `objeto` con opciones adicionales (este campo __no__ es requerido).
    + zoom: `number` __default:__ `12`


## Definición:

```js
fn.centerMap = function (lat, long, options) {};
```


## Un ejemplo:

```js
Anuket.centerMap(51.4926659, -0.1583277);
```

ó definiendo el parámetro `zoom`


```js
Anuket.centerMap(51.4926659, -0.1583277, {zoom: 14});
```
# getOriginPosition

Devuelve un `objeto` con las coordenadas definidas al inicio de la ejecución (método [run](run.md)).


## Lista de Parámetros:

- `Ninguno`


## Definición:
```js
fn.getOriginPosition = function() {};
```


## Un ejemplo:

```js
Anuket.getOriginPosition();
//{lat: 51.5180094, lng: -0.7637034}
```
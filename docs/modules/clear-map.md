# clearMap

Elimina todos los `marcadores`, `polígonos` y `polilíneas` que se encuentren en el mapa y agrega un marcador (opcional) en la localización geográfica definida al inicio de la ejecución (método [run](run.md)).


## Lista de Parámetros:

- __options__: `object` __default:__ `null` un objeto con opciones adicionales (este campo no es requerido).
    + origin: `boolean` __default:__ `false`
    + marker: `boolean` __default:__ `false`


## Definición:

```js
fn.clearMap = function (options) {};
```


## Un ejemplo:

```js
Anuket.clearMap();
```

con las opciones establecidas:

```js
Anuket.clearMap({origin: true, marker: true});
```


## Consideraciones:

- Como se comentó arriba, el objeto **options** ysus valores son opcionales, por lo tanto pueden enviarse los que considere necesarios.

- De forma automática el método `clearMap` invoca a las siguientes funciones:
    + `removePolylines`
    + `removeMarkers`
    + `removePolygons`
    + `removeOverlay`

- Si se envía el campo `origin`, invoca internamente al método [centerOverOrigin](center-over-origin.md).
- Sí se envía el campo `marker`, agrega un marcador en la localización geográfica definida al inicio de la ejecución (método [run](run.md)).


!!! note "Nota:"
    Recomendamos leer la [documentación de gmaps,](https://hpneo.dev/gmaps/documentation.html) para más información.
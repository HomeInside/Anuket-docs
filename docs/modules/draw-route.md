# drawRoute

Permite dibujar una ruta entre dos puntos, usando polilíneas:


## Lista de Parámetros:
  - __origin__: `Array` __default:__ `[]`
  - __destiny__: `Array` __default:__ `[]`
  - __options:__ un `objeto` con opciones adicionales (este campo __no__ es requerido).
      + travelMode: `string` __default:__ `null`
      + strokeColor: `string` __default:__ `null`
      + strokeOpacity: `float` __default:__ `null`
      + strokeWeight: `integer` __default:__ `null`
      + markers: `boolean` __default:__ `true`


## Definición:

```js
fn.drawRoute = function(origin, destiny, options) {};
```


## Ejemplos:

- Con las opciones por defecto:

```js
Anuket.drawRoute([51.51455, -0.07306], [51.598186, 0.057406]);
```

- Definiendo opciones:

```js
var options = {
  travelMode: 'driving',
  strokeColor: '#0E51DC',
  strokeOpacity: 0.8,
  strokeWeight: 7,
  markers: true
}

Anuket.drawRoute([51.51455, -0.07306], [51.598186, 0.057406], options);
```


## Consideraciones:

- La opción `travelMode` acepta __driving__, __bicycling__ ó __walking__.

- La opción `strokeColor` permite cambiar el color de la polilínea. en formato hexadecimal.

- La opción `strokeOpacity` permite cambiar la opacidad de la polilínea en valores de __0.0__ a __1.0__.

- La opción `strokeWeight` permite cambiar el ancho de la polilínea en píxeles.

- La opción `markers` permite ubicar un [marcador](add-markers.md) al inicio y fin de la ruta.

!!! tip
    Ten en cuenta que este método, dibuja la ruta teniendo en cuenta el tráfico y las vías de acceso disponibles y están sujetas a los [Límites y políticas de uso](https://developers.google.com/maps/documentation/javascript/directions?hl=es-419#UsageLimits).

!!! note "Nota:"
    Recomendamos leer el apartado de [Servicio de indicaciones,](https://developers.google.com/maps/documentation/javascript/directions?hl=es-419) y [Formas](https://developers.google.com/maps/documentation/javascript/shapes?hl=es-419#polylines) para más información.

  
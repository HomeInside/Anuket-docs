# drawPolygon :new:

Permite dibujar un polígono en el mapa, usando polilíneas:

!!! note "Nota:"
    Este método, sólo se encuentra disponible en la version __3.0.b.__ ó superior.


## Lista de Parámetros:
- __paths__: `Array` __default:__ `[]`
- __options:__ un `objeto` con opciones adicionales (este campo __no__ es requerido).
    + strokeColor: `string` __default:__ `null`
    + strokeOpacity: `integer` __default:__ `null`
    + strokeWeight: `integer` __default:__ `null`
    + fillColor: `string` __default:__ `null`
    + fillOpacity: `float` __default:__ `null`
    + toRemove: `string` __default:__ `null`


## Definición:

```js
fn.drawPolygon = function(paths, options) {};
```


## Ejemplos:

- Con las opciones por defecto:

```js
var paths=[[-.10106027126312256,51.49660768717089],[-.10135263204574585,51.49625868955697],[-.1007947325706482,51.495752721763886],[-.10040313005447388,51.49601155087454],[-.10031193494796753,51.495993182405485],[-.1001080870628357,51.496305445373196],[-.10021001100540161,51.49633884235538],[-.10018318891525267,51.49638726793613],[-.10106027126312256,51.49660768717089]];

Anuket.drawPolygon(paths);
```

- Definiendo opciones:

```js
var options = {
  strokeColor: '#0E51DC',
  strokeOpacity: 1,
  strokeWeight: 2,
  fillColor: '#0E51DC',
  fillOpacity: 0.2,
  toRemove: 'all',
};

Anuket.drawPolygon(paths, options);
```


## Consideraciones:

- La opción `strokeColor` permite cambiar el color de contorno, del polígono, en formato hexadecimal.

- La opción `strokeOpacity` permite cambiar la opacidad del color de contorno, del polígono.

- La opción `strokeWeight` permite cambiar el ancho de la línea de contorno, en píxeles.

- La opción `fillColor` permite cambiar el color de relleno del polígono, en formato hexadecimal.

- La opción `fillOpacity` permite cambiar la opacidad del color de relleno, del polígono, en valores de __0.0__ a __1.0__.

- La opción `toRemove` acepta: __all__, __polygons__ ó __polylines__. Dependiendo la opción escogida, el método __drawPolygon__ ejecutará las siguientes acciones:
    + __all__: Limpiará el mapa.
    + __polygons__: Borrará todos los polígonos existentes.
    + __polylines__: Borrará todas las polilíneas existentes.
    
    antes de dibujar el nuevo polígono.


!!! note "Nota:"
    Recomendamos leer el apartado de [polígonos en Google Maps](https://developers.google.com/maps/documentation/javascript/examples/polygon-simple) y [la Api asociada](https://developers.google.com/maps/documentation/javascript/reference/3.36/polygon) para más información.

  
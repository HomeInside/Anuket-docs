# Anuket-docs

Documentación de __Anuket__, Awesome [Google maps](https://developers.google.com/maps/?hl=es-419) Utils.


## Versión estable
Esta documentación corresponde a la versión __3.1.3__ (May 10, 2022)

Puedes descargar la versión estable del proyecto [por aquí](https://gitlab.com/HomeInside/Anuket), ó revisa los lanzamientos mas recientes, [por aquí](https://gitlab.com/HomeInside/Anuket/-/tags)


## Listado de Cambios
El listado completo de cambios (changelog), [por aquí](help/CHANGELOG.md)


## Licencia
El texto completo de la licencia puede ser encontrado en el archivo __MIT-LICENSE.txt__.


## Contacto
- [Diniremix](https://gitlab.com/diniremix)
- [Duverney](https://gitlab.com/duverney.ortiz)